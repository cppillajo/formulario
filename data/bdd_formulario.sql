-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-02-2020 a las 05:19:49
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdd_formulario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_formulario`
--

CREATE TABLE `tbl_formulario` (
  `f_id` int(50) NOT NULL,
  `f_fecha` varchar(100) NOT NULL,
  `f_dirigido_a` varchar(200) NOT NULL,
  `f_solicitante` varchar(200) NOT NULL,
  `f_cedula_solicitante` varchar(100) NOT NULL,
  `f_representante_de` varchar(200) NOT NULL,
  `f_grado_o_curso` varchar(100) NOT NULL,
  `f_asunto` varchar(500) NOT NULL,
  `f_estado` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_formulario`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_formulario_solicitud`
--

CREATE TABLE `tbl_formulario_solicitud` (
  `fs_id` int(50) NOT NULL,
  `f_id` int(50) NOT NULL,
  `fs_resolucion` varchar(200) NOT NULL,
  `fs_estado` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_formulario_solicitud`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_solicitud_documento`
--

CREATE TABLE `tbl_solicitud_documento` (
  `sd_id` int(50) NOT NULL,
  `f_id` int(50) NOT NULL,
  `sd_periodo_matricula` varchar(100) NOT NULL,
  `sd_matricula` varchar(100) NOT NULL,
  `sd_periodo_promocion` varchar(100) NOT NULL,
  `sd_promocion` varchar(100) NOT NULL,
  `sd_estado` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_solicitud_documento`
--

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_formulario`
--
ALTER TABLE `tbl_formulario`
  ADD PRIMARY KEY (`f_id`);

--
-- Indices de la tabla `tbl_formulario_solicitud`
--
ALTER TABLE `tbl_formulario_solicitud`
  ADD PRIMARY KEY (`fs_id`);

--
-- Indices de la tabla `tbl_solicitud_documento`
--
ALTER TABLE `tbl_solicitud_documento`
  ADD PRIMARY KEY (`sd_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_formulario`
--
ALTER TABLE `tbl_formulario`
  MODIFY `f_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_formulario_solicitud`
--
ALTER TABLE `tbl_formulario_solicitud`
  MODIFY `fs_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_solicitud_documento`
--
ALTER TABLE `tbl_solicitud_documento`
  MODIFY `sd_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
