<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class formulario_documento_model extends CI_Model {
	

	public function save($data)
	{
		$this->db->insert("tbl_solicitud_documento",$data);
	}

	public function select_where($f_id){
		$this->db->select("tbl_solicitud_documento.*");
		$this->db->from("tbl_solicitud_documento");
		$this->db->where("tbl_solicitud_documento.f_id",$f_id);
		$respuesta = $this->db->get();
		return $respuesta->result();
	}
	
}
