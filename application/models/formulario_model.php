<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class formulario_model extends CI_Model {
	

	public function save($data)
	{
		$this->db->insert("tbl_formulario",$data);
	}
	public function select_max()
	{
		$this->db->select("MAX(f_id) as 'f_id'");
		$this->db->from("tbl_formulario");
		$respuesta = $this->db->get();
		return $respuesta->row();
	}

	public function select(){
		$this->db->select("tbl_formulario.*");
		$this->db->from("tbl_formulario");
		$respuesta = $this->db->get();
		return $respuesta->result();
	}

	public function update($data,$f_id){
		$this->db->where("tbl_formulario.f_id",$f_id);
		$this->db->update("tbl_formulario",$data);

	}

}
