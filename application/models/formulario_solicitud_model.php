<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class formulario_solicitud_model extends CI_Model {
	

	public function save($data)
	{
		$this->db->insert("tbl_formulario_solicitud",$data);
	}

	public function select_where($f_id){
		$this->db->select("tbl_formulario_solicitud.*");
		$this->db->from("tbl_formulario_solicitud");
		$this->db->where("tbl_formulario_solicitud.f_id",$f_id);
		$respuesta = $this->db->get();
		return $respuesta->result();
	}

}
