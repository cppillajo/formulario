<br><div class="row" style="display: flex;justify-content: center;"  >
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">FORMULARIO DE SOLICITUD</h3>
                                <form class="mt-3" action="<?php echo base_url();?>index.php/formulario/formulario_solicitud/insert" method="post">
                                    <div class="form-group">
                                        
                                        <h5 >Solicitante</h5>
                                         <input type="text" class="form-control" placeholder="Ingrese nombre y apellido del solicitante" id="solicitante" name="solicitante" >
                                         <br>
                                        <h5 >Cedula del solicitante</h5>
                                         <input type="text" class="form-control" placeholder="Ingrese la cedula del solicitante" id="cedula" name="cedula" >
                                          <br>
                                         <h5 >Representante De</h5>
                                         <input type="text" class="form-control" placeholder="Ingrese nombre y apellido del Representante " id="representante_de" name="representante_de">
                                          <br>
                                          <h5 >Curso</h5>
                                         <input type="text" class="form-control" placeholder="Ingrese el curso" id="curso" name="curso" >
                                          <br>
                                          <h5 >Asunto</h5>
                                        <textarea class="form-control" rows="3" id="asunto" name="asunto" ></textarea>

                                    </div>
                                    <div class="btn-list">
                                        <button   type="submit"
                                        class="btn waves-effect waves-light btn-rounded btn-outline-success">Guardar</button>                                          
                                    </div>  
                                </form>

                            </div>
                        </div>
                    </div>
</div>
