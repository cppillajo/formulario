<div class="container-fluid">
    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">TODOS LOS FORMULARIOS</h4>                                
                                    <div class="table-responsive">
                                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>RESOLUCION</th>
                                                                                 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                       <?php if(!empty($datos)):?>
                                                       <?php foreach($datos as $item):?>
                                                <tr>
                                                    <td><?php echo $item->fs_id;?></td>
                                                    <td><?php echo $item->fs_resolucion;?></td>            
                                                </tr>
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>
</div>
<a href="<?php echo base_url()?>index.php/formulario/formulario_admin" class="btn waves-effect waves-light btn-rounded btn-outline-success">REGRESAR</a>