<div class="container-fluid">
    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">TODOS LOS FORMULARIOS</h4>                                
                                    <div class="table-responsive">
                                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>FECHA</th>
                                                    <th>DIRIGIDO A</th>
                                                    <th>SOLICITANTE</th>
                                                    <th>CEDULA</th>
                                                    <th>REPRESENTANTE DE</th>
                                                    <th>CURSO</th>
                                                    <th>ASUNTO</th>
                                                    <th>ESTADO</th>
                                                    <th>OPCIONES</th>                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                                       <?php if(!empty($datos)):?>
                                                       <?php foreach($datos as $item):?>
                                                <tr>
                                                    <td><?php echo $item->f_id;?></td>
                                                    <td><?php echo $item->f_fecha;?></td>
                                                    <td><?php echo $item->f_dirigido_a;?></td>
                                                    <td><?php echo $item->f_solicitante;?></td>
                                                    <td><?php echo $item->f_cedula_solicitante;?></td>
                                                    <td><?php echo $item->f_representante_de;?></td>
                                                    <td><?php echo $item->f_grado_o_curso;?></td>
                                                    <td><?php echo $item->f_asunto;?></td> 
                                                    <td><?php if($item->f_estado==1){echo "AUTORIZADO";}else{echo "NO AUTORIZADO";}  ?></td>          
                                                    <td>
                                                        <div class="btn-group">                          
                                                            <a href="<?php echo base_url()?>index.php/formulario/formulario_admin/buscar/<?php echo $item->f_id;?>" class="btn btn-warning"><span class="fas fa-search"></span></a>

                                                            <a href="<?php echo base_url()?>index.php/formulario/formulario_admin/autorizar/<?php echo $item->f_id;?>" class="btn btn-primary"><span class="fas fa-check"></span></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>
</div>