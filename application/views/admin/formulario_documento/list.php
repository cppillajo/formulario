<br><div class="row" style="display: flex;justify-content: center;"  >
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">SOLICITUD DE DOCUMENTO</h3>
                                <form class="mt-3" action="<?php echo base_url();?>index.php/formulario/formulario_documento/insert" method="post">
                                    <div class="form-group">
                                        <h5 >Solicitante</h5>
                                         <input type="text" class="form-control" placeholder="Ingrese nombre y apellido del solicitante" id="solicitante" name="solicitante">
                                         <br>
                                        <h5 >Cedula del solicitante</h5>
                                         <input type="text" class="form-control" placeholder="Ingrese la cedula del solicitante" id="cedula" name="cedula" >
                                          <br>
                                         <h5 >Representante De</h5>
                                         <input type="text" class="form-control" placeholder="Ingrese nombre y apellido del Representante " id="representante_de" name="representante_de">
                                          <br>
                                          <h5 >Curso</h5>
                                         <input type="text" class="form-control" placeholder="Ingrese el curso" id="curso" name="curso">
                                          <br>
                                          <h5 >Asunto</h5>
                                          <textarea class="form-control" rows="3" id="asunto" name="asunto"></textarea>
                                          <br>

                      
                                <h4 class="card-title">Seleccione</h4>
                                                                     
                                        <select class="form-control" onclick="mostrar_tipo()"                id="seleccionar_tipo" >
                                            <option value="0">SELECCIONAR...</option>
                                            <option value="1">MATRICULA</option>
                                            <option value="2">PROMOCION</option>
                                            <option value="3">MATRICULA--PROMOCION</option>
                                        </select>
                                 
                           

                                    <div class="card" id="matricula"  style="display: none">
                                        <div class="card-body">
                                            <h5 class="card-title" >Matricula</h5>
                                          <input type="text" class="form-control" placeholder="Ingrese el numero de matricula" id="numero_matricula" name="numero_matricula" >
                                          <h5 >Periodo Academico</h5> 
                                          <textarea class="form-control" rows="3" placeholder="INGRESAR:                                                                                              Curso                                  año lectivo  " id="periodo_academico_matricula" name="periodo_academico_matricula" ></textarea>
                                          
                                        </div>
                                    </div>

                                    <div class="card" id="promocion" style="display: none">
                                        <div class="card-body">
                                            <h5 class="card-title" >Promociones</h5>
                                          <input type="text" class="form-control" placeholder="Ingrese el numero de promoción" name="numero_promocion" id="numero_promocion" >
                                          <h5 >Periodo Academico</h5> 
                                          <textarea class="form-control" rows="3" placeholder="INGRESAR:                                                                                      Curso(5to)                                    año lectivo(2016-2017)  " id="periodo_academico_promocion" name="periodo_academico_promocion" ></textarea>
                                          
                                        </div>
                                    </div>

                                    </div>
                                    <div class="btn-list">
                                        <button   type="submit"
                                        class="btn waves-effect waves-light btn-rounded btn-outline-success">Guardar</button>                                        
                                     </div>  
                                </form>
                            </div>
                        </div>
                    </div>
</div>
