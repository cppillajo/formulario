<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <!-- Logo icon -->
                        <a href="<?php echo base_url();?>">
                            <b class="logo-icon">
                                <!-- Dark Logo icon -->
                                <img src="<?php echo base_url();?>assets/images/MARCA_HORIZONTAL_V1.png" alt="homepage" class="dark-logo" style="width:200px;height: 75px" /  >                                
                               
                            </b>
                            <!--End Logo icon -->
                            
                          
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" style="display: flex;justify-content: center;">
                    <b><h2>FORMULARIO DE: SOLICITUD DE DOCUMENTOS Y FORMULARIO DE DOCUMENTOS</h2></b>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<?php echo base_url();?>"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">INICIO</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Formularios</span></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><i data-feather="file-text" class="feather-icon"></i>
                                <span class="hide-menu">Formularios </span></a>
                            
                                
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url();?>index.php/formulario/formulario_solicitud" class="sidebar-link">
                                    <span class="hide-menu"> Formulario Solicitud </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="<?php echo base_url();?>index.php/formulario/formulario_documento" class="sidebar-link">
                                    <span class="hide-menu"> Solicitud Documento </span>
                                    </a>
                                </li>                                
                          
                        </li> 
                        
                    </ul>
                    <div style="display: none" id="admin_formulario_vistas">
                        <ul id="sidebarnav">
                            <li class="list-divider"></li>
                            <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                    aria-expanded="false"><i data-feather="box" class="feather-icon"></i>
                                    <span class="hide-menu">ADMINISTRACIÓN</span></a>
                                <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                    
                                    <li class="sidebar-item">
                                        <a href="<?php echo base_url();?>index.php/formulario/formulario_admin" class="sidebar-link">
                                        <span class="hide-menu"> Formularios  </span>
                                        </a>
                                    </li>                                                                   
                                </ul>
                            </li> 
                        </ul>    
                    </div>
                    <ul id="sidebarnav">
                            <li class="list-divider"></li>
                            <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                    aria-expanded="false"><i data-feather="lock" class="feather-icon"></i>
                                    <span class="hide-menu">LOGIN</span></a>
                                <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                    <small>uso solo del administrador</small>
                                    <input type="password" name="contraseña_admin" id="contraseña_admin"  placeholder="INGRESE LA CONTRASEÑA" class="form-control">
                                    <input type="button" onclick="login();" value="LOGIN" class="btn waves-effect waves-light btn-rounded btn-outline-success">                                                                  
                                </ul>
                            </li> 
                    </ul>  
                     
                     
                        
          
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">