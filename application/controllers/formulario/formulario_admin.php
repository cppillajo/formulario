<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class formulario_admin extends CI_Controller {
	
	public function __construct(){
		parent::__construct();			
		$this->load->model("formulario_model");
		$this->load->model("formulario_solicitud_model");
		$this->load->model("formulario_documento_model");
	}

	public function index()
	{
		$data = array('datos' => $this->formulario_model->select() );
		$this->load->view('layouts/header.php');
		$this->load->view('layouts/aside');
		$this->load->view('admin/formulario/list',$data);
		$this->load->view('layouts/footer');
	}

	public function buscar($f_id)
	{	

		$respuesta_solicitud = $this->formulario_solicitud_model->select_where($f_id);
		$respuesta_documento = $this->formulario_documento_model->select_where($f_id);
		
		if($respuesta_solicitud != null){
			$data = array('datos' => $respuesta_solicitud);
			$this->load->view('layouts/header.php');
			$this->load->view('layouts/aside');
			$this->load->view('admin/formulario/list_solicitud',$data);
			$this->load->view('layouts/footer');

		}else{
			if($respuesta_documento != null){
				$data = array('datos' => $respuesta_documento);
				$this->load->view('layouts/header.php');
				$this->load->view('layouts/aside');
				$this->load->view('admin/formulario/list_documento',$data);
				$this->load->view('layouts/footer');

			}
		}

		
	}
	public function autorizar($f_id)
	{	
			$data = array('f_estado' => 1);			
			$this->formulario_model->update($data,$f_id);
			$data = array('datos' => $this->formulario_model->select() );
			$this->load->view('layouts/header.php');
			$this->load->view('layouts/aside');
			$this->load->view('admin/formulario/list',$data);
			$this->load->view('layouts/footer');

		

		
	}
	
}
