<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class formulario_documento extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model("formulario_model");	
		$this->load->model("formulario_documento_model");		
		
	}

	public function index()
	{
		$this->load->view('layouts/header.php');
		$this->load->view('layouts/aside');
		$this->load->view('admin/formulario_documento/list');
		$this->load->view('layouts/footer');
	}
	public function insert()
	{

		$solicitante= $this->input->post("solicitante");
		$cedula= $this->input->post("cedula");
		$representante_de= $this->input->post("representante_de");
		$curso= $this->input->post("curso");
		$asunto= $this->input->post("asunto");
		$fecha=date("Y-m-d",time());
		$dirigido_a="Msc.Norma Yánez";
		
		

		$numero_matricula= $this->input->post("numero_matricula");
		$periodo_academico_matricula= $this->input->post("periodo_academico_matricula");
		$numero_promocion= $this->input->post("numero_promocion");
		$periodo_academico_promocion= $this->input->post("periodo_academico_promocion");

		if($this->validarCI($cedula)==true){
			if($solicitante != "" && $cedula != "" && $representante_de != "" && $curso != "" && $asunto != "" && $fecha != "" && $dirigido_a != ""   ){

			if($numero_matricula != "" || $numero_promocion !=""){
				if($periodo_academico_matricula != "" || $periodo_academico_promocion != ""){

					$data= array('f_fecha' => $fecha ,
								 'f_dirigido_a' => $dirigido_a ,
								 'f_solicitante' => $solicitante,
								 'f_cedula_solicitante' =>$cedula ,
								 'f_representante_de' => $representante_de,
								 'f_grado_o_curso' =>$curso ,
								 'f_asunto' =>$asunto,
								 'f_estado' => 	0
					);
					$this->formulario_model->save($data);
				  	$respuesta= $this->formulario_model->select_max();
				 	

				 	$data= array('f_id' => $respuesta->f_id,
				 				 'sd_periodo_matricula' => $periodo_academico_matricula ,
				 				 'sd_matricula' => $numero_matricula ,
				 				 'sd_periodo_promocion' => $periodo_academico_promocion,
				 				 'sd_promocion' =>$numero_promocion ,
				 				 'sd_estado' => 1			 	
				 	);
				 	$this->formulario_documento_model->save($data);

				 	echo "<b>UNIDAD EDUCATIVA CATÓLICA LA VICTORIA</b>";
					echo "<br><small>Misioneras y Misioneros Identes</small>";
					echo "<br><br><b>Codigo:  </b>".$respuesta->f_id;
					echo "<br><b>fecha:  </b>".$fecha;
					echo "<br><b>Dirigido A:  </b>".$dirigido_a;
					echo "<br><b>Solicitante:  </b>".$solicitante;
					echo "<br><b>Representante de:  </b>".$representante_de;
					echo "<br><b>Grado / Curso:  </b>".$curso;
					echo "<br><b>Asunto:  </b>".$asunto;

					if($numero_matricula != "" && $periodo_academico_matricula!="" ){
						echo "<br><b>Matricula:  </b>".$numero_matricula;
						echo "<br><b>Periodo Academico:  </b>".$periodo_academico_matricula;
					}
					if($numero_promocion != "" && $periodo_academico_promocion!="" ){
						echo "<br><b>Promocion:  </b>".$numero_promocion;
						echo "<br><b>Periodo Academico:  </b>".$periodo_academico_promocion;
					}


					echo " 
					<br>  <input type='button' onclick='Imprimir()' value='imprimir'>
					<br><a href='".base_url()."' >REGRESAR AL INICIO</a>


					<script> 
					function Imprimir(){					
					if(window.print)window.print();
					}</script>";
					


				}else{
					echo "<script> alert('Ingrese año lectivo de matricula o promocion')</script>";
					$this->load->view('layouts/header.php');
					$this->load->view('layouts/aside');
					$this->load->view('admin/formulario_documento/list');
					$this->load->view('layouts/footer');
				}
				
			}else{
			    echo "<script> alert('Ingrese numero de matricula o promocion')</script>";
			    $this->load->view('layouts/header.php');
				$this->load->view('layouts/aside');
				$this->load->view('admin/formulario_documento/list');
				$this->load->view('layouts/footer');
			}
			
			
		}else{
			echo "<script> alert('Ingrese todos los datos para la solicutd')</script>";
			$this->load->view('layouts/header.php');
			$this->load->view('layouts/aside');
			$this->load->view('admin/formulario_documento/list',$regresar_datos);
			$this->load->view('layouts/footer');
		}
			
		}else{
			echo "<script> alert('CEDULA INCORRECTA')</script>";
			$this->load->view('layouts/header.php');
			$this->load->view('layouts/aside');
			$this->load->view('admin/formulario_documento/list');
			$this->load->view('layouts/footer');
		}
		
		


		
	}
	function validarCI($strCedula)
	{
	//aqui explico la logica de la validacion de una cedula de ecuador
	//El decimo Digito es un resultante de un calculo
	//Se trabaja con los 9 digitos de la cedula
	//Cada digito de posicion impar se lo duplica, si este es mayor que 9 se resta 9
	//Se suman todos los resultados de posicion impar
	//Ahora se suman todos los digitos de posicion par
	//se suman los dos resultados
	//se resta de la decena inmediata superior
	//este es el decimo digito
	//si la suma nos resulta 10, el decimo digito es cero

	if(is_null($strCedula) || empty($strCedula)){//compruebo si que el numero enviado es vacio o null
	//echo "Por Favor Ingrese la Cedula";
	}else{//caso contrario sigo el proceso
	if(is_numeric($strCedula)){
	$total_caracteres=strlen($strCedula);// se suma el total de caracteres
	if($total_caracteres==10){//compruebo que tenga 10 digitos la cedula
	$nro_region=substr($strCedula, 0,2);//extraigo los dos primeros caracteres de izq a der
	if($nro_region>=1 && $nro_region<=24){// compruebo a que region pertenece esta cedula//
	$ult_digito=substr($strCedula, -1,1);//extraigo el ultimo digito de la cedula
	//extraigo los valores pares//
	$valor2=substr($strCedula, 1, 1);
	$valor4=substr($strCedula, 3, 1);
	$valor6=substr($strCedula, 5, 1);
	$valor8=substr($strCedula, 7, 1);
	$suma_pares=($valor2 + $valor4 + $valor6 + $valor8);
	//extraigo los valores impares//
	$valor1=substr($strCedula, 0, 1);
	$valor1=($valor1 * 2);
	if($valor1>9){ $valor1=($valor1 - 9); }else{ }
	$valor3=substr($strCedula, 2, 1);
	$valor3=($valor3 * 2);
	if($valor3>9){ $valor3=($valor3 - 9); }else{ }
	$valor5=substr($strCedula, 4, 1);
	$valor5=($valor5 * 2);
	if($valor5>9){ $valor5=($valor5 - 9); }else{ }
	$valor7=substr($strCedula, 6, 1);
	$valor7=($valor7 * 2);
	if($valor7>9){ $valor7=($valor7 - 9); }else{ }
	$valor9=substr($strCedula, 8, 1);
	$valor9=($valor9 * 2);
	if($valor9>9){ $valor9=($valor9 - 9); }else{ }

	$suma_impares=($valor1 + $valor3 + $valor5 + $valor7 + $valor9);
	$suma=($suma_pares + $suma_impares);
	$dis=substr($suma, 0,1);//extraigo el primer numero de la suma
	$dis=(($dis + 1)* 10);//luego ese numero lo multiplico x 10, consiguiendo asi la decena inmediata superior
	$digito=($dis - $suma);
	if($digito==10){ $digito='0'; }else{ }//si la suma nos resulta 10, el decimo digito es cero
	if ($digito==$ult_digito){//comparo los digitos final y ultimo
	   //echo "Cedula Correcta";
	    return true;
	}else{
	   // echo "Cedula Incorrecta";
	    return false;
	}
	}else{
	//echo "Este Nro de Cedula no corresponde a ninguna provincia del ecuador";
	}


	}else{
	//echo "Es un Numero y tiene solo".$total_caracteres;
	}
	}else{
	//echo "Esta Cedula no corresponde a un Nro de Cedula de Ecuador";
	}
	}
	}
}
